import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListProductosComponent } from './components/list-productos/list-productos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ViewProductoComponent } from './components/view-producto/view-producto.component';
import { BuscadorProductosComponent } from './components/buscador-productos/buscador-productos.component';
import { SearchPipe } from './pipes/search.pipe';
import { FilterCategoriesComponent } from './components/filter-categories/filter-categories.component';
import { FilterPipe } from './pipes/filter.pipe';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    ListProductosComponent,
    ViewProductoComponent,
    BuscadorProductosComponent,
    SearchPipe,
    FilterPipe,
    FilterCategoriesComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
