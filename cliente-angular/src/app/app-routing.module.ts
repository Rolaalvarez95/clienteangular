import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListProductosComponent } from './components/list-productos/list-productos.component';
import { ViewProductoComponent } from './components/view-producto/view-producto.component';


const routes: Routes = [
  { path: '', component: ListProductosComponent },
  { path: 'view-producto/:id', component: ViewProductoComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
