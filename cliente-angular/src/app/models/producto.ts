export class Producto {
  _id: string;
  CODIGO: string;
  NOMBRE: string;
  UNITID: string;
  PRECIO_VENTA: number;
  COSTO_FINANCIERO: string;
  IDLINEA: string;
  IDCATEGORIA: string;
  CODIDPROVEEDOR: string;
  ESTADO: string;
  TIPOFABRICANTE: string;
  NUMERO_PARTE: string;
  LINEA: string;
  CATEGORIA: string;
  UEN: string;
  MARCA: string;
  PRECIO_MINIMO: number;
  DESCRIPCION: string;
  IMAGEN_150: string;
  IMAGEN_450: string;

  constructor(_id: string, CODIGO: string, NOMBRE: string,
    UNITID: string,
    PRECIO_VENTA: number,
    COSTO_FINANCIERO: string,
    IDLINEA: string,
    IDCATEGORIA: string,
    CODIDPROVEEDOR: string,
    ESTADO: string,
    TIPOFABRICANTE: string,
    NUMERO_PARTE: string,
    LINEA: string,
    CATEGORIA: string,
    UEN: string,
    MARCA: string,
    PRECIO_MINIMO: number,
    DESCRIPCION: string,
    IMAGEN_150: string,
    IMAGEN_450: string,) {
    this._id = _id
    this.CODIGO = CODIGO
    this.NOMBRE = NOMBRE
    this.UNITID = UNITID
    this.PRECIO_VENTA = PRECIO_VENTA
    this.COSTO_FINANCIERO = COSTO_FINANCIERO
    this.IDLINEA = IDLINEA
    this.IDCATEGORIA = IDCATEGORIA
    this.CODIDPROVEEDOR = CODIDPROVEEDOR
    this.ESTADO = ESTADO
    this.TIPOFABRICANTE = TIPOFABRICANTE
    this.NUMERO_PARTE = NUMERO_PARTE
    this.LINEA = LINEA
    this.CATEGORIA = CATEGORIA
    this.UEN = UEN
    this.MARCA = MARCA
    this.PRECIO_MINIMO = PRECIO_MINIMO
    this.DESCRIPCION = DESCRIPCION
    this.IMAGEN_150 = IMAGEN_150
    this.IMAGEN_450 = IMAGEN_450

  }
}
