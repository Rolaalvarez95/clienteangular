import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { ProductosService } from 'src/app/services/productos.service';


@Component({
  selector: 'app-list-productos',
  templateUrl: './list-productos.component.html',
  styleUrls: ['./list-productos.component.css']
})
export class ListProductosComponent implements OnInit {
  filtroProducto = ""
  filtroCategoria = ""
  listProductos: Producto[] = []
  showSpinner: boolean = true;
  filterProductos(value: string) {

    this.filtroProducto = value
  }
  filtrarCategorias(value: string) {
    this.filtroCategoria = value
  }




  constructor(private _productoService: ProductosService) {

  }
  ngOnInit(): void {

    this.obtenerProductos();


  }


  obtenerProductos() {
    this._productoService.getProductos().subscribe(data => {
      this.listProductos = data
      this.showSpinner = false
    }, error => {
      console.log(error);

    })
  }



}
