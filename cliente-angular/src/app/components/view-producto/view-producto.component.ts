import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from 'src/app/models/producto';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-view-producto',
  templateUrl: './view-producto.component.html',
  styleUrls: ['./view-producto.component.css']
})
export class ViewProductoComponent implements OnInit {
  id?: string
  producto?: Producto
  showSpinner: boolean = true
  constructor(private _productoService: ProductosService, private aRouter: ActivatedRoute) {
    this.id = this.aRouter.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.obtenerProducto(this.id)

  }
  obtenerProducto(id: string) {
    if (id != null) {
      this._productoService.obtenerProducto(id).subscribe(data => {
        this.producto = data
        this.showSpinner = false
      })
    }
  }

}
