import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms'
import { debounceTime } from 'rxjs/operators'

@Component({
  selector: 'app-buscador-productos',
  templateUrl: './buscador-productos.component.html',
  styleUrls: ['./buscador-productos.component.css']
})
export class BuscadorProductosComponent implements OnInit {

  constructor() { }
  filterProductos = new FormControl('');
  ngOnInit(): void {
    this.filterProductos.valueChanges.pipe(debounceTime(300)).subscribe(value => {
      this.searchProducto.emit(value)
    })
  }

  @Output('search') searchProducto = new EventEmitter<string>();

}
