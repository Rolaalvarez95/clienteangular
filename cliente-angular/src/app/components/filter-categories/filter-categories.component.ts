import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Categoria } from 'src/app/models/categories';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-filter-categories',
  templateUrl: './filter-categories.component.html',
  styleUrls: ['./filter-categories.component.css']
})
export class FilterCategoriesComponent implements OnInit {
  listCategorias: Categoria[] = []
  constructor(private _categoriaServices: ProductosService) { }
  filtrarCategorias = new FormControl('');
  ngOnInit(): void {
    this.obtenerCategorias();
    this.filtrarCategorias.valueChanges.subscribe(value => {
      this.filterCategoria.emit(value)
    })
  }

  obtenerCategorias() {
    this._categoriaServices.getCategorias().subscribe(data => {
      this.listCategorias = data
    }, error => {
      console.log(error);

    })
  }


  @Output('filter') filterCategoria = new EventEmitter<string>();



}
