import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  url = "http://pacific-headland-97417.herokuapp.com/"

  constructor(private http: HttpClient) { }
  getProductos(): Observable<any> {
    return this.http.get(this.url + 'productos/');
  }
  obtenerProducto(id: string): Observable<any> {
    return this.http.get(this.url + 'producto/' + id);
  }
  getCategorias(): Observable<any> {
    return this.http.get(this.url + 'categorias/');
  }

}
